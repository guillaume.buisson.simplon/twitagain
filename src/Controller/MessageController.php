<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Entity\Hashtags;

class MessageController extends AbstractController
{


    /**
     * @Route("/api/messages", name="getAllByOrder", methods={"GET"})
     */
    public function getAllMessage(EntityManagerInterface $entityManager)
    {

        $messages = $entityManager->getRepository(Messages::class)->findBy([], ['createdat' => 'DESC']);

        $result = [];
        foreach ($messages as $message) {

            $com = [];
            $comments = $message->getComments();
            foreach ($comments as $comment){

                $com[] = [
                  'idComment'=>$comment->getId(),
                  'comment'=>$comment->getTextcom(),
                  'date'=>$comment->getCreatedat(),
                  'user'=>$comment->getUser()->getId(),
                  'nom'=>$comment->getUser()->getNom(),
                  'prenom'=>$comment->getUser()->getPrenom(),
                  'photo'=>$comment->getUser()->getImage(),


                ];
            }

            $com = [];
            $comments = $message->getComments();
            foreach ($comments as $comment){

                $com[] = [
                    'comment'=>$comment->getTextcom(),
                    'date'=>$comment->getCreatedat(),
                    'user'=>$comment->getUser()->getId(),
                    'nom'=>$comment->getUser()->getNom(),
                    'prenom'=>$comment->getUser()->getPrenom(),
                    'photo'=>$comment->getUser()->getImage(),


                ];
            }

            $hts = [];
            $hashtags = $message->getHashtags();
            foreach ($hashtags as $hashtag){

                $hts[] = [
                    'id'=>$hashtag->getId(),
                    'content'=>$hashtag->getTexthashtag(),
                ];
            }

            $result[] = [
                // Message content
                'id' => $message->getId(),
                'content' => $message->getContent(),
                'createdat' => $message->getCreatedat(),
                'image' => $message->getImage(),
                // Message Author
                'user' => [
                    'id' => $message->getAuthor()->getId(),
                    'username' => $message->getAuthor()->getUsername(),
                    'nom' => $message->getAuthor()->getNom(),
                    'prenom' => $message->getAuthor()->getPrenom(),
                    'image'=>$message->getAuthor()->getImage()
                ],
                // Message Likes
                'likes' => [
                    'count' => count($message->getLikes()),
                    'me' => $message->getLikes()->contains($this->getUser())
                ],
                // Message Comment
                'comment'=>[
                    'comText'=>$com

                ],
                // Message Hashtag
                'hashtags'=>[
                    'hashtag'=>$hts
                ]
            ];
        }

        return $this->json(['messages' => $result]);
    }

    /**
     * @Route("/api/messages/{id}", name="getMessagebyId", methods={"GET"})
     */
    public function getMessageById(EntityManagerInterface $entityManager, $id)
    {
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }
        $arrayofmessage = [];

        $com = [];
        $comments = $message->getComments();
        foreach ($comments as $comment){

            $com[] = [
                'idComment'=>$comment->getId(),
                'comment'=>$comment->getTextcom(),
                'date'=>$comment->getCreatedat(),
                'user'=>$comment->getUser()->getId(),
                'nom'=>$comment->getUser()->getNom(),
                'prenom'=>$comment->getUser()->getPrenom(),
                'photo'=>$comment->getUser()->getImage(),
             ];
            }

        $hts = [];
        $hashtags = $message->getHashtags();
        foreach ($hashtags as $hashtag){

            $hts[] = [
                'id'=>$hashtag->getId(),
                'content'=>$hashtag->getTexthashtag(),
            ];
        }
            $arraymessage = [
                'id' => $message->getId(),
                'content' => $message->getContent(),
                'createdat' => $message->getCreatedat(),
                    'user' => [
                         'id'=> $message->getAuthor()->getId(),
                         'username' => $message->getAuthor()->getUsername(),
                         'nom' => $message->getAuthor()->getNom(),
                         'prenom' => $message->getAuthor()->getPrenom(),
                         'image'=>$message->getAuthor()->getImage()
                        ],
                'likes' => [
                    'count' => count($message->getLikes()),
                    'me' => $message->getLikes()->contains($this->getUser())
                ],
                // Message Comment
                'comment'=>[
                    'comText'=>$com

                ],
                'hashtags'=>[
                    'hashtag'=>$hts
                ]
        ];
        $arrayofmessage[] = $arraymessage;

        return $this->json([
            'messages' => $arrayofmessage,
        ]);
    }

    /**
     * @Route("/messages/view", name="viewMessage")
     */
    public function viewMessage()
    {

        return $this->render('home/index.html.twig', [
             ]);

    }

    /**
     * @Route("/api/messages/{id}", name="updateMessagebyId", methods={"PUT"})
     */
    public function UpdateMessageById(EntityManagerInterface $entityManager,Request $request, $id)
    {
        $message = $entityManager->getRepository(Messages::class)->find($id);

        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }

        $content = $request->request->get('content');
        $image = $request->request->get('image');

        $message->setContent($content);
        $message->setImage($image);
        $message->setCreatedat(new \DateTime());

        $entityManager->persist($message);
        $entityManager->flush();


        return $this->json([
            'status' => 'correctly updated',
        ]);
    }

    /**
     * @Route("/api/messages/{id}", name="removeMessagebyId", methods={"DELETE"})
     */
    public function removeMessageById(EntityManagerInterface $entityManager, $id)
    {
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }

        $entityManager->remove($message);
        $entityManager->flush();

        return $this->json([
            'mess' => 'Delete successful',
        ]);
    }

    /**
     * @Route("/api/messages", name="addMessage", methods={"POST"})
     */
    public function addMessage(EntityManagerInterface $entityManager, Request $request)
    {
        $content = $request->request->get('content');
        $id = $request->request->get('user');
        $hts = $request->request->get('htgs');
        $user = $entityManager->getRepository(Users::class)->find($id);

        if($user === null) {
            return $this->json([
                'error' => 'User with id #'.$id.' not found.',
            ], 404);
        }

        $message = new Messages();
        $message->setContent($content);
        $message->setAuthor($user);
        $message->setCreatedat(new \DateTime());

        if($hts != null) {
            foreach ($hts as $ht) {
                $hashtag = new Hashtags();
                $hashtag->setTexthashtag($ht);
                $message->addHashtag($hashtag);

                $entityManager->persist($hashtag);
                $entityManager->persist($message);
            }
        }
        else {
            $entityManager->persist($message);
        }

        $entityManager->flush();

        return $this->json([
            'message' => 'correctly created.',
            'id' => $message->getId()
        ], 201);
    }




    /**
     * @Route("/api/messages/{id}/likes", name="getLikebyIdMessage", methods={"GET"})
     */
    public function getLikeByMessages(EntityManagerInterface $entityManager, $id)
    {
        $arrayofuser = [];
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }
        $like = $message->getLikes();

        foreach ($like as $user) {
            $id = $user->getId();
            $arrayofuser[] = $id;
        };

        $arraymessage = [
            'id'=>$message->getId(),
            'likes'=>[
                'nbrs' => count($message->getLikes()),
                'authors' => $arrayofuser,
            ]
        ];

        return $this->json([
            'message' => $arraymessage,
        ]);
    }

    /**
     * @Route("/api/messages/{id}/retweets", name="getRtbyIdMessage", methods={"GET"})
     */
    public function getRtByMessages(EntityManagerInterface $entityManager, $id)
    {
        $arrayofuser = [];
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }

        $Rt = $message->getRetweets();

        foreach ($Rt as $user) {
            $id = $user->getUsername();
            $arrayofuser[] = $id;
        };

        $arraymessage = [
            'id'=>$message->getId(),
            'retweet'=>[
                'nbrs' => count($message->getRetweets()),
                'authors' => $arrayofuser,
            ]
        ];

        return $this->json([
            'message' => $arraymessage,
        ]);
    }

    /**
         * @Route("/api/messages/{id}/comments", name="getAllCommentsbyIdMessage", methods={"GET"})
     */
    public function getCommentsByMessages(EntityManagerInterface $entityManager, $id)
    {
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }
        $comments = $message->getComments();
        $mycommentarray = [];

        foreach ($comments as $comment) {
            $commentarray = [
                'id'=>$comment->getId(),
                'content'=>$comment->getTextcom(),
                'author'=>$comment->getUser()->getId(),
                'nom'=>$comment->getUser()->getNom(),
                'prenom'=>$comment->getUser()->getPrenom(),
                'photo'=>$comment->getUser()->getImage(),
                'username'=>$comment->getUser()->getUsername(),
                'messageId'=>$comment->getMessage()->getId(),
                'date'=>$comment->getMessage()->getCreatedat()


            ];
            $mycommentarray[] = $commentarray;
        }

        $arraymessage = [
            'id'=>$message->getId(),
            'comments'=>$mycommentarray
        ];

        return $this->json([
            'message' => $arraymessage,
        ]);
    }


    /**
     * @Route("/api/messages/{id}/hashtags", name="getHtagsbyIdMessage", methods={"GET"})
     */
    public function getHtagsByMessages(EntityManagerInterface $entityManager, $id)
    {
        $arrayofhtag = [];
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }

        $htags = $message->getHashtags();

        foreach ($htags as $htag) {
            $content = $htag->getTexthashtag();
            $arrayofhtag[] = $content;
        };

        $arraymessage = [
            'id'=>$message->getId(),
            'hashtags'=>[
                'nbrs' => count($message->getHashtags()),
                'texts' => $arrayofhtag,
            ]
        ];

        return $this->json([
            'message' => $arraymessage,
        ]);
    }
}
