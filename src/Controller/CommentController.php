<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\Messages;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class CommentController extends AbstractController
{
    /**
     * @Route("/api/comments", name="addComments", methods={"POST"})
     */
    public function addComments(EntityManagerInterface $entityManager, Request $request) {
        $content = $request->request->get('content');
        $userid = $request->request->get('user_id');
        $user = $entityManager->getRepository(Users::class)->find($userid);

        if($user === null) {
            return $this->json([
                'error' => 'User with id #'.$userid.' not found.',
            ], 404);
        }

        $messageid = $request->request->get('message_id');
        $message = $entityManager->getRepository(Messages::class)->find($messageid);

        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$messageid.' not found.',
            ], 404);
        }

        $comment = new Comments();
        $comment->setTextcom($content);
        $comment->setCreatedat(new \DateTime());
        $comment->setUser($user);
        $comment->setMessage($message);

        $entityManager->persist($comment);
        $entityManager->flush();


        return $this->json([
            'comment'=>'successful',
            'id' => $comment->getId()
        ]);
    }

    /**
     * @Route("/api/comments/{id}", name="updateCommentbyId", methods={"PUT"})
     */
    public function updateCommentbyId(EntityManagerInterface $entityManager,Request $request, $id)
    {
        $comment = $entityManager->getRepository(Comments::class)->find($id);
        if($comment === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }

        $content = $request->request->get('content');

        $comment->setTextcom($content);
        $comment->setCreatedat(new \DateTime());

        $entityManager->persist($comment);
        $entityManager->flush();


        return $this->json([
            'status' => 'correctly updated',
        ]);
    }

    /**
     * @Route("/api/comments/{id}", name="RemoveComments", methods={"DELETE"})
     */
    public function RemoveComment(EntityManagerInterface $entityManager, Request $request,$id) {

        $comment = $entityManager->getRepository(Comments::class)->find($id);
        if($comment === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }

        $entityManager->remove($comment);
        $entityManager->flush();

        return $this->json([
            'comment'=>'successfully deleted'
        ]);
    }
}
