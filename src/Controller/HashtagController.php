<?php

namespace App\Controller;

use App\Entity\Hashtags;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HashtagController extends AbstractController
{
    /**
     * @Route("/api/hashtags/{id}/messages", name="findMessageByHashtag")
     */
    public function getMessagesFromHashtag(EntityManagerInterface $entityManager, $id): Response
    {
        $arrayofmessages = [];
        $hashtag = $entityManager->getRepository(Hashtags::class)->find($id);
        $messages = $hashtag->getMsgid();

        foreach ($messages as $message) {
            $arrayofmessages[] = [
                'id'=> $message->getId(),
                'content'=>$message->getContent(),
                'date'=>$message->getCreatedat(),
                'user' => [
                    'id' => $message->getAuthor()->getId(),
                    'username' => $message->getAuthor()->getUsername(),
                    'nom' => $message->getAuthor()->getNom(),
                    'prenom' => $message->getAuthor()->getPrenom(),
                    'image'=>$message->getAuthor()->getImage()
                ],
                'likes' => [
                    'count' => count($message->getLikes()),
                    'me' => $message->getLikes()->contains($this->getUser())
                ],
            ];
        }

        $arraymessage = [
            'id'=> $hashtag->getId(),
            'hashtag'=> $hashtag->getTexthashtag(),
            'messages'=> $arrayofmessages,

        ];

        return $this->json([
            'hashtag' => $arraymessage,
        ]);
    }

    /**
     * @Route("/api/hashtags/{id}", name="UpdateHashtag", methods={"PUT"})
     */
    public function UpdateHashtag(EntityManagerInterface $entityManager, Request $request, $id) {
        $ht = $entityManager->getRepository(Hashtags::class)->find($id);

        $content = $request->request->get('content');

        $ht->setTexthashtag($content);

        $entityManager->persist($ht);
        $entityManager->flush();

        return $this->json([
           'status'=>'correctly updated',
        ]);
    }

    /**
     * @Route("/api/hashtags/{id}", name="DeleteHashtag", methods={"DELETE"})
     */
    public function DeleteHashtag(EntityManagerInterface $entityManager, Request $request, $id) {
        $ht = $entityManager->getRepository(Hashtags::class)->find($id);

        $entityManager->remove($ht);
        $entityManager->flush();

        return $this->json([
            'status'=>'correctly updated',
        ]);
    }
}
