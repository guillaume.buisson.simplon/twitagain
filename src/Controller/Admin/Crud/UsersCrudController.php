<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Users;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UsersCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Users::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            EmailField::new('email'),
            TextField::new('nom'),
            TextField::new('prenom'),
            TextField::new('username'),
            ArrayField::new('roles'),
            TextField::new('image')->hideOnIndex(),
            AssociationField::new('messages'),
            AssociationField::new('likes'),
            AssociationField::new('mentions'),
            AssociationField::new('retweets'),
            AssociationField::new('comments'),
            AssociationField::new('followers'),
        ];
    }
}
