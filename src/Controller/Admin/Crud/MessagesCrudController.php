<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Messages;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MessagesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Messages::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextareaField::new('content'),
            TextField::new('image')->hideOnIndex(),
            AssociationField::new('author'),
            AssociationField::new('likes'),
            AssociationField::new('retweets'),
            AssociationField::new('comments'),
            AssociationField::new('hashtags'),
        ];
    }

}
