<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Hashtags;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class HashtagsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Hashtags::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('texthashtag'),
            AssociationField::new('msgid')
        ];
    }

}
