<?php

namespace App\Controller\Admin;

use App\Entity\Comments;
use App\Entity\Hashtags;
use App\Entity\Messages;
use App\Entity\Users;
use App\Repository\HashtagsRepository;
use App\Repository\MessagesRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $entityManager = $this->get('doctrine');

        /** @var UsersRepository $userRepository */
        $userRepository = $entityManager->getRepository(Users::class);
        $totalUsers = $userRepository->count([]);
        $totalUsersWithMsg = $userRepository->countUsersWithMessages();
        $bestUsers = $userRepository->findUsersByMessageCount();
        $mostFollowers = $userRepository->findUserWithMostFollowers();

        /** @var MessagesRepository $messageRepository */
        $messageRepository = $entityManager->getRepository(Messages::class);
        $totalMessages = $messageRepository->count([]);
        $mostLikes = $messageRepository->findMessageWithMostLikes();

        /** @var HashtagsRepository $hashtagRepository */
        $hashtagRepository = $entityManager->getRepository(Hashtags::class);
        $totalHashtags = $hashtagRepository->count([]);
        $bestHashtags = $hashtagRepository->findMostUsedHashtags();

        return $this->render('admin/dashboard.html.twig', [
            'users' => [
                'best' => $bestUsers,
                'total' => $totalUsers,
                'totalWithMsg' => $totalUsersWithMsg,
                'mostFollowers' => $mostFollowers,
            ],
            'messages' => [
                'total' => $totalMessages,
                'mostLikes' => $mostLikes
            ],
            'hashtags' => [
                'total' => $totalHashtags,
                'best' => $bestHashtags,
            ]
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Twitagain');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),
            MenuItem::linkToCrud('Users', null, Users::class),
            MenuItem::linkToCrud('Messages', null, Messages::class),
            MenuItem::linkToCrud('Hashtags', null, Hashtags::class),
            MenuItem::linkToCrud('Comments', null, Comments::class),
        ];
    }
}
