<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Validator\Constraints\Date;

class UserController extends AbstractController
{
    /**
     * @Route("/api/users", name="takeusers", methods={"GET"})
     */
    public function takeUser(EntityManagerInterface $entityManager, Request $request)
    {
        $users = $entityManager->getRepository(Users::class)->findAll();
        $arrayofusers = [];

        foreach ($users as $user) {
            $arrayuser = ['id'=>$user->getId(),
                             'email'=>$user->getEmail(),
                             'nom'=>$user->getNom(),
                             'prenom'=>$user->getPrenom(),
                             'username'=>$user->getUsername()
            ];
            $arrayofusers[] = $arrayuser;
        }


        return $this->json([
            'users' => $arrayofusers,
    ]);

    }

    /**
     * @Route("/api/users", name="addUsers", methods={"POST"})
     */
    public function addUser(EntityManagerInterface $entityManager, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $email = $request->request->get('email');
        $nom = $request->request->get('nom');
        $prenom = $request->request->get('prenom');
        $pwd = $request->request->get('pwd');
        $username = $request->request->get('username');

        $user = new Users();
        $user->setEmail($email);
        $user->setNom($nom);
        $user->setPrenom($prenom);
        $user->setRoles(["ROLE_USER"]);
        $user->setPassword($passwordEncoder->encodePassword($user, $pwd));
        $user->setUsername($username);
        $user->setCreatedat(new \DateTime());

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json([
            'users' => 'correctly created.',
            'id' => $user->getId()
        ], 201);

    }

    /**
     * @Route("/api/users/{id}", name="takeoneuser", methods={"GET"})
     */
    public function takeOneUser(EntityManagerInterface $entityManager, Request $request, $id)
    {
        $users = $entityManager->getRepository(Users::class)->find($id);
        if($users === null) {
            return $this->json([
                'error' => 'User with id #'.$id.' not found.',
            ], 404);
        }

        $arrayuser = ['id'=>$users->getId(),
            'email'=>$users->getEmail(),
            'nom'=>$users->getNom(),
            'prenom'=>$users->getPrenom(),
            'username'=>$users->getUsername(),
            'date'=>$users->getCreatedat(),
            'image'=>$users->getImage()
        ];


        return $this->json([
            'users' => $arrayuser,
        ]);

    }

    /**
     * @Route("/api/users/{id}", name="removeOneUser", methods={"DELETE"})
     */
    public function removeOneUser(EntityManagerInterface $entityManager, Request $request, $id)
    {
        $user = $entityManager->getRepository(Users::class)->find($id);
        if($user === null) {
            return $this->json([
                'error' => 'User with id #'.$id.' not found.',
            ], 404);
        }
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->json([
            'users' => 'correctly deleted',
        ]);

    }

    /**
     * @Route("/api/users/{id}/messages", name="getMessagebyUserId", methods={"GET"})
     */
    public function getMessageByUserId(EntityManagerInterface $entityManager, $id)
    {
        $messages = $entityManager->getRepository(Messages::class)->findMessageByUserId($id);
        $arrayofmessages = [];

        foreach ($messages as $message) {


            $arraymessage = ['id'=>$message->getId(),
                'content'=>$message->getContent(),
                'Date'=>$message->getCreatedAt(),
                'author'=>$message->getAuthor()->getPrenom(),

                'user' => [
                    'id' => $message->getAuthor()->getId(),
                    'username' => $message->getAuthor()->getUsername(),
                    'nom' => $message->getAuthor()->getNom(),
                    'prenom' => $message->getAuthor()->getPrenom(),
                    'image' => $message->getAuthor()->getImage()
                ],
            ];
            $arrayofmessages[] = $arraymessage;
        }

        return $this->json([
            'messages'=>$arrayofmessages,
        ]);
    }

    /**
     * @Route("/api/users/{id}/followers", name="getFollowersbyUserId", methods={"GET"})
     */
    public function getFollowersByUserId(EntityManagerInterface $entityManager, $id)
    {
        $users = $entityManager->getRepository(Users::class)->find($id);
        if($users === null) {
            return $this->json([
                'error' => 'User with id #'.$id.' not found.',
            ], 404);
        }
        $arrayofuser = [];
        $followers = $users->getFollowers();

        foreach ($followers as $user) {
            $id = $user->getId();
            $arrayofuser[] = $id;
        };

        $arrayfollowers = [
            'id'=>$users->getId(),
            'followers'=>[
                'nbrs' => count($users->getFollowers()),
                'authors' => $arrayofuser,
            ]
        ];

        return $this->json([
            'user'=>$arrayfollowers,
        ]);
    }

    /**
     * @Route("/api/users/{id}", name="updateUsername", methods={"PUT"})
     */
    public function updateUsername(EntityManagerInterface $entityManager, Request $request, $id) {
        $username = $request->request->get('username');
        $image = $request->request->get('image');
        $user = $entityManager->getRepository(Users::class)->find($id);

        if($user === null) {
            return $this->json([
                'error' => 'User with id #'.$id.' not found.',
            ], 404);
        }

        if($username != null && $image == null) {
            $user->setUsername($username);
        }
        elseif ($image != null && $username == null) {
            $user->setImage($image);
        }

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json([
            'user'=>'successfully updated',
        ]);
    }
}
