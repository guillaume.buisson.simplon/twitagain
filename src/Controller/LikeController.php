<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LikeController extends AbstractController
{
    /**
     * @Route("/api/like", name="AddLike", methods={"POST"})
     */
    public function addLike(EntityManagerInterface $em , Request $request): Response
    {
        $userid = $request->request->get('user_id');
        $user = $em->getRepository(Users::class)->find($userid);
        if($user === null) {
            return $this->json([
                'error' => 'User with id #'.$userid.' not found.',
            ], 404);
        }

        $messageid = $request->request->get('message_id');
        $message = $em->getRepository(Messages::class)->find($messageid);

        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$messageid.' not found.',
            ], 404);
        }


        $message->addLike($user);

        $em->persist($message);
        $em->flush();


        return $this->json( [
                'like_success'=>'Message like'
        ]);
    }

    /**
     * @Route("/api/likes/{id}", name="RemoveLikes", methods={"DELETE"})
     */
    public function removeLikes(EntityManagerInterface $entityManager, Request $request,$id) {
        $userid = $request->request->get('user_id');
        $message = $entityManager->getRepository(Messages::class)->find($id);
        if($message === null) {
            return $this->json([
                'error' => 'Message with id #'.$id.' not found.',
            ], 404);
        }
        $likes = $message->getLikes();

        foreach ($likes as $like) {
            if($like->getId() === $userid) {
                $entityManager->remove($like);
            }
        }

        $entityManager->flush();

        return $this->json([
            'comment'=>'successfully deleted'
        ]);
    }
}
