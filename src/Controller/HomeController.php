<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/perso/{id}", name="perso")
     */
    public function perso(int $id): Response
    {
        return $this->render('home/perso.html.twig', [
           'id' => $id
        ]);

    }

    /**
     * @Route("/", name="home")
     */
    public function home(): Response
    {
        return $this->render('home/perso.html.twig');

    }
}
