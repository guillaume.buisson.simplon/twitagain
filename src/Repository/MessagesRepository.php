<?php

namespace App\Repository;

use App\Entity\Messages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class MessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Messages::class);
    }

    /**
    * @return Messages[] Returns an array of Messages objects
    */
    public function findMessageByUserId($id): array
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.author = :userid')
            ->setParameter('userid', $id)
            ->orderBy('m.createdat', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findMessageWithMostLikes($max = 10)
    {
        return $this->createQueryBuilder('m')
            ->addSelect('COUNT(l.id) as nbLikes')
            ->join('m.likes', 'l')
            ->groupBy('m.id')
            ->addOrderBy('COUNT(l.id)')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }
}
