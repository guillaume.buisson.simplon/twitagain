<?php

namespace App\DataFixtures;

use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new Users();
        $user->setEmail('admin@caramail.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'admin'));
        $user->setNom('John');
        $user->setPrenom('Doe');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setUsername('jdoe');
        $user->setCreatedat(new \DateTime());

        $manager->persist($user);
        $manager->flush();
    }
}
