<?php

namespace App\DataFixtures;

use App\Entity\Comments;
use App\Entity\Messages;
use App\Entity\Users;
use Container8ZnhjJ3\getMaker_AutoCommand_MakeEntityService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use http\Message;
use Symfony\Component\Validator\Constraints\Date;

class UsersFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR'); //ajouter la class
        // installer faker : composer require fzaninotto/faker --dev
        //php bin/console doctrine:fixtures:load
        // $product = new Product();
        // $manager->persist($product);
        for ($i=0 ; $i< 30 ; $i++){
            $photo = "https://picsum.photos/id/10/200/300";
        $user = new Users();
        $user->setEmail($faker->email);
        $user->setPassword('$2y$10$Z2yxRYSac0gJYyyd6vErrOPIUN2o.0ZdsZvWyyDbd93wsa4ZFnoUK');
        $user->setPrenom($faker->firstName);
        $user->setNom($faker->lastName);
        $user->setCreatedat(new \DateTime());
        $user->setUsername($faker->userName);
        $user->setImage($photo);
        $user->setRoles(["USER"]);

        $message = new Messages();
        $message->setCreatedat($faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = null));
        $message->setAuthor($user);
        $message->setContent($faker->sentence());

        $comments = new Comments();
        $comments->setTextcom($faker->sentence());
        $comments->setMessage($message);
        $comments->setUser($user);


        $manager->persist($user);
        $manager->persist($message);
        $manager->persist($comments);


        }
        $manager->flush();






    }
}
