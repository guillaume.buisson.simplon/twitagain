<?php

namespace App\Entity;

use App\Repository\MessagesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MessagesRepository::class)
 */
class Messages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class, inversedBy="likes")
     * @ORM\JoinTable(name="likes")
     */
    private $likes;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class, inversedBy="mentions")
     * @ORM\JoinTable(name="mentions")
     */
    private $mentions;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class, inversedBy="retweets")
     * @ORM\JoinTable(name="retweets")
     */
    private $retweets;

    /**
     * @ORM\ManyToMany(targetEntity=Hashtags::class, mappedBy="msgid")
     */
    private $hashtags;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Comments::class, mappedBy="message", orphanRemoval=true, cascade={"persist"})
     */
    private $comments;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
        $this->mentions = new ArrayCollection();
        $this->retweets = new ArrayCollection();
        $this->hashtags = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getAuthor(): ?Users
    {
        return $this->author;
    }

    public function setAuthor(?Users $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Users $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
        }

        return $this;
    }

    public function removeLike(Users $like): self
    {
        $this->likes->removeElement($like);

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getMentions(): Collection
    {
        return $this->mentions;
    }

    public function addMention(Users $mention): self
    {
        if (!$this->mentions->contains($mention)) {
            $this->mentions[] = $mention;
        }

        return $this;
    }

    public function removeMention(Users $mention): self
    {
        $this->mentions->removeElement($mention);

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getRetweets(): Collection
    {
        return $this->retweets;
    }

    public function addRetweet(Users $retweet): self
    {
        if (!$this->retweets->contains($retweet)) {
            $this->retweets[] = $retweet;
        }

        return $this;
    }

    public function removeRetweet(Users $retweet): self
    {
        $this->retweets->removeElement($retweet);

        return $this;
    }

    /**
     * @return Collection|Hashtags[]
     */
    public function getHashtags(): Collection
    {
        return $this->hashtags;
    }

    public function addHashtag(Hashtags $hashtag): self
    {
        if (!$this->hashtags->contains($hashtag)) {
            $this->hashtags[] = $hashtag;
            $hashtag->addMsgid($this);
        }

        return $this;
    }

    public function removeHashtag(Hashtags $hashtag): self
    {
        if ($this->hashtags->removeElement($hashtag)) {
            $hashtag->removeMsgid($this);
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComments(Comments $comments): self
    {
        if (!$this->comments->contains($comments)) {
            $this->comments[] = $comments;
            $comments->setMessage($this);
        }

        return $this;
    }

    public function removeComments(Comments $comments): self
    {
        if ($this->comments->removeElement($comments)) {
            // set the owning side to null (unless already changed)
            if ($comments->getMessage() === $this) {
                $comments->setMessage(null);
            }
        }

        return $this;
    }
}
