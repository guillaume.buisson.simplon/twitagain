<?php

namespace App\Entity;

use App\Repository\HashtagsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HashtagsRepository::class)
 */
class Hashtags
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $texthashtag;

    /**
     * @ORM\ManyToMany(targetEntity=Messages::class, inversedBy="hashtags")
     */
    private $msgid;


    public function __construct()
    {
        $this->msgid = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->texthashtag;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexthashtag(): ?string
    {
        return $this->texthashtag;
    }

    public function setTexthashtag(string $texthashtag): self
    {
        $this->texthashtag = $texthashtag;

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getMsgid(): Collection
    {
        return $this->msgid;
    }

    public function addMsgid(Messages $msgid): self
    {
        if (!$this->msgid->contains($msgid)) {
            $this->msgid[] = $msgid;
        }

        return $this;
    }

    public function removeMsgid(Messages $msgid): self
    {
        $this->msgid->removeElement($msgid);

        return $this;
    }
}
