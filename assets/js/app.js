import '@fortawesome/fontawesome-free/css/all.min.css'
import '../scss/app.scss'
import $ from 'jquery'
require("@fortawesome/fontawesome-free/css/all.min.css")
require("@fortawesome/fontawesome-free/js/all.js")
let idUser = localStorage.getItem('idUser')
let forAll = `/api/messages`
let forMoi = `/api/users/${idUser}/messages`
let forHim = `/api/messages/`
let post
let forHt = ""
let datasetting
let hashtags = []
let firstCom
let htcontent = []
let htid
let inputPut
let readMsg = false
let pencilPutCom
let idMessage
let content
let date
let author
let nom
let view
let image
let prenom
let username
let comNom
let comPrenom
let comPhoto
let nbLike
let nbCom
let readCom = `<span id="comListe" data-id="${idMessage}">  liste des commentaires: <i class="fas fa-caret-down"></i></span>`
let commentaire
let comsComment = {}
let comsDate
let comsUser
let id
let idCom
let correllation = false
let crossDelCom = `<i class="fas fa-times crossCom"></i>`
let profilmodif = false


//let forMoi2 = forMe + idUser

function getUrlVars() {
    let vars = {};
    let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


$('#profil').hide()
$('#profil').fadeOut('slow')

$('.circleId').click(function(e){
    alert(idUser)
})


$('#profile').click(function(){
    profil(idUser)
    $('#profil').animate({top:'5vh'})
    $('#profile').css('color','cadetblue')

})



$(document).on('click','.cross',  function(){

    $('#profil').animate({top:'-100vh'})
    $('#profile').css('color','white')
});


$(document).on('click','.circleIdPost',  function(e){
    let idProfilUser = $(this).data('id')
    profil(idProfilUser)
    $('#profil').animate({top:'5vh'})
});



$(document).on('click','.fa-comment',  function(){

    let idProfilUser = $(this).data('id')


    if($(`#inputCommentCache${idProfilUser}`).hasClass('openInput'))
    {
        $(`#inputCommentCache${idProfilUser}`).removeClass('openInput')
    }else{
        $(`#inputCommentCache${idProfilUser}`).addClass('openInput')

    }



});












// Login Form
$('#formLogin').submit(function(e){
    e.preventDefault();
    let ident = $('#emailConnec').val()
    let pass = $('#passConnec').val()

    $.ajax({
        type: "POST",
        url: "/connexionAjax",
        data: JSON.stringify({ email: ident, password: pass }),
        contentType: "application/json; charset=utf-8",
        datatype: 'json',

        success: function(msg){
            const profileId = msg.id
            localStorage.setItem('idUser', profileId);  //à mettre en local storage
            if (msg.result === "Bienvenue"){
                message("Connexion Réussie ")
                window.location.href=`/perso/${profileId}`
            }else{
                message('Erreur de Mot de Passe')
            }
        }
    })
})


// Register Form
$('#formRegister').submit(function(e) {
    e.preventDefault();
    let email = $('#emailSign').val()
    let pwd = $('#pwdSign').val()
    let nom = $('#nomSign').val()
    let prenom = $('#prenomSign').val()
    let username = $('#userName').val()


    $.ajax({
        type: "POST",
        url: "/api/users/add",
        data: {'email': email, 'pwd': pwd, 'nom': nom, 'prenom': prenom, 'username': username},
        datatype: 'json',

        success: function (msg) {
            message('Compte créer')
            window.location.href = "/connexion";
        },
        error: function(){
            message("erreur dans le formulaire")
        }
    })

})

const profil = (id) => {
    if (id === idUser) {
        $('.modif').show()
    } else {
        $('.modif').hide()
    }
    $.getJSON(`/api/users/${id}`, function (data3) {
        $('#nomProfil').html(data3.users.nom)
        $('#prenomProfil').html(data3.users.prenom)
        $('#emailProfil').html(data3.users.email)
        $('#usernameProfil').html(data3.users.username)
        $('#photoProfil').attr('src', `${data3.users.image}`)
        $('#dateProfil').html((new Date(data3.users.date)).toLocaleString())
        $('#profil').show().fadeIn('slow')
    })
}

const message = (text) => {
    $('.validationCreation').html(text).css('opacity', '1')
    const fade = () => {
        $('.validationCreation').css('opacity', '0')
    }
    setTimeout(fade, 2000)
}

$('.icoSize').click(function () {
    const tweet = $('.inputSeach').val()
    if(tweet != "") {
        let htArray = [];
        console.log($('#addhtDiv').children());
        for (let i = 0; i < $('#addhtDiv').children().length; i++) {
            htArray.push($('#addhtDiv').children()[i].innerText);
        }
        console.log(htArray);
        $('.inputSeach').val("");
        $('#htInput').val("");
        $('#addhtDiv').empty();
        $.ajax({
            type: "POST",
            url: "/api/messages",
            data: {'user': idUser, 'content': tweet, 'htgs': htArray},
            datatype: 'json',

            success: function(msg){
                refeshHome(forAll)

            }
        })
    }
    else {
        alert('input vide');
     }
})


$(document).on('click','#addhtButton',  function() {
    let reg =  /[^\w]/gi
    let phrase = $('#htInput').val();
    phrase = phrase.replace(reg, '')
    if($('#htInput').val() != "") {
        console.log(phrase);
        $('#addhtDiv').append(`<span class="htTag">#${phrase}</span>`);
    } else {
        alert('input vide');
    }
    $('#htInput').val("");
})
$('#home').click(function(){
    $('#htTitle').text("");
    refeshHome(forAll)
})





$('#mesTweets').click(function(){
    $('#htTitle').text("");
    refeshHome(forMoi)
})
$(document).on('click','.linkHt',  function(e){
    let idht = $(this).attr('id');
    console.log(idht);
    $('#postSection').empty()
    forHt = `/api/hashtags/${idht}/messages`;
    $('#htTitle').text($(this).text());
    refeshHome(forHt);

})


let refreshCompteurs = (idMessage) => {
    $.get(`/api/messages/${idMessage}/comments`, function (data) {
        let dataCount = data.message.comments.length
        $(`.nbComCount${idMessage}`).html(`<i data-id="${idMessage}" class="far rot25Deg transition fa-comment"></i>&nbsp ${dataCount}`
        )

    })}


let commentList = (idCom) => {
    $(`#insertComForMess${idCom}`).empty()
    $.get(`/api/messages/${idCom}/comments`, function (data) {
        let i = data.message.comments.length

        for(  i=i-1 ; i > -1  ; i--){


            author = data.message.comments[i].author
            content = data.message.comments[i].content
            comNom = data.message.comments[i].nom
            comPhoto = data.message.comments[i].photo
            comPrenom = data.message.comments[i].prenom
            username = data.message.comments[i].username
            date = data.message.comments[i].date
            id = data.message.comments[i].id
            console.log(data.message.comments[i])
            if(author == idUser){
                crossDelCom = `<i data-id="${idCom}" data-idm="${id}" class="fas fa-times"></i>`
                pencilPutCom = `<i data-id="${idCom}" class="far fa-edit"></i>`
                inputPut = ` <form id="sendCommentEdit${id}" class="sendCommentEdit hide" data-id="${idCom}" data-idm="${id}"> 
      <div id="containInput"><input id="inputPut${id}" class=" comPut" type="text" placeholder="Editer votre Commentaire" comPut" id="inputPut${id}">
      <button id="btnSendCommentEdit" type="submit" data-id="${id}" data-idmsg="${idCom}" class="fas  paperPlaneComment fa-paper-plane"></button></div>

      </form>`
            }else{
                crossDelCom = ''
                pencilPutCom = ''
                inputPut = ''
            }

            $(`#insertComForMess${idCom}`).append(`
            
<div id="firstComm">
    <div id="firstUserCom">
    <img class="circleIdPostCom" src="${comPhoto}">
    <div id="profilRepCom">
        ${comNom} &nbsp &nbsp<br/>
        ${comPrenom} &nbsp &nbsp
        
    </div>

      <div id="emplacementcom">

      <div id="date">le ${date}</div>
      <div id="content">${content}</div>
      ${inputPut}
      </div>
      <div class="crossCom" data-idm="${idCom}" data-id="${id}">
      ${crossDelCom}
      </div>
        <div class="pencilPut" data-idm="${idCom}" data-id="${id}">
         ${pencilPutCom}
      </div>

    </div>
</div>
            
            
            `)


        }

    })
}

$(document).on('click','.pencilPut',  function() {
    let idm = $(this).data('id')
    if($(`#sendCommentEdit${idm}`).hasClass('hide')){

        $(`#sendCommentEdit${idm}`).removeClass('hide')
    }else{
        $(`#sendCommentEdit${idm}`).addClass('hide')
    }
})




$(document).on('click','.crossCom',  function(e) {
    let comId = $(this).data('id')
    let messageId = $(this).data('idm')


    $.ajax({
        url: `/api/comments/${comId}`,
        type: 'DELETE',
        success: function(comment) {
            commentList(messageId)
        }
    });
})


$(document).on('click','#view',  function(e) {
    readMsg = true
    let id = $(this).data('idmsg')
    $('#firstContainerRow').empty()
    window.location.href = `/messages/view?id=${id}`
    refeshHome(forHim + id)
})

$('#disconnect').click(function(){
    window.location.href = "/logout";
})

$(document).on('click','#btnSendCommentEdit',  function(e) {
    e.preventDefault();
    let idm = $(this).data('idmsg')
    let id = $(this).data('id')
    let data = $(`#inputPut${id}`).val()

    $.ajax({
        type: "PUT",
        url: `/api/comments/${id}`,
        data: {'content': data},

        success: function (msg) {

            commentList(idm)
            refreshCompteurs(idm)
        },
        error: function () {
            console.log("Ce message n'existe plus")
        }
    })
})

$(document).on('submit','.sendCommentEdit',  function(e) {
    e.preventDefault();
    let idm = $(this).data('idm')
    let id = $(this).data('id')
    let data = $(`#inputPut${idm}`).val()

    $.ajax({
        type: "PUT",
        url: `/api/comments/${idm}`,
        data: {'content': data},

        success: function (msg) {

            commentList(id)
            refreshCompteurs(id)
        },
        error: function(){
            console.log("Ce message n'existe plus")
        }
    })


})
$(document).on('submit','#sendComment',  function(e){
    e.preventDefault();
    let messageId = $(this).data('id')
    let input = '#inputComment' + messageId
    let content = $(`${input}`).val()


    $.ajax({
        type: "POST",
        url: "/api/comments",
        data: {'content': content, 'user_id': idUser, 'message_id': messageId },
        datatype: 'json',

        success: function (msg) {

            message("Comment sent")
            commentList(messageId)
            refreshCompteurs(messageId)
        },
        error: function(){
            message(idUser)
        }
    })
})

$(document).on('click','#comListe',  function(e) {
    let id = $(this).data('id')
    let idComList = $(this).data('idCom')
    if($('.fa-caret-down').hasClass('rotate180')){
        $(`#insertComForMess${id}`).empty()

        $('.fa-caret-down').removeClass('rotate180')

    }else{
        $('.fa-caret-down').addClass('rotate180')
        commentList(id)
        refreshCompteurs(id)

    }
})

let setImageProfil = () => {
    $.get(`/api/users/${idUser}`, function (data) {
        const ImageProfil = data.users.image
        $('.circleId').attr('src', `${ImageProfil}`)

    })
}


let refeshHome = (forwho) => {
    $('#postSection').empty()

    $.get(forwho, function( data ) {
        console.log(data)
         correllation = false;

        if(forwho === forHt) {
            datasetting = data.hashtag.messages;
            htid = data.hashtag.id;
            hashtags = data.hashtag.hashtag;
        }
        else {
            datasetting = data.messages;
        }

        datasetting.forEach(message => {
            if(forwho == forMoi) {
                idMessage = message.id
                content = message.content
                date = new Date(message.Date)
                author = message.author
            }else{

                 idMessage = message.id
                 content = message.content
                 date = new Date(message.createdat)
                 author = message.user.id
                 nom = message.user.nom
                 prenom = message.user.prenom
                 username = message.user.username
                 image = message.user.image
                 nbLike = message.likes.count
                 correllation = message.likes.me
                comsComment = message.comment.comText
                nbCom = message.comment.comText.length
                comsComment.forEach(com => {
                    idCom = com.idComment
                    commentaire = com.comment
                    comsUser = com.user
                    comsDate = com.date
                    comNom = com.nom
                    comPrenom = com.prenom
                    comPhoto = com.photo
                })

                hashtags = message.hashtags.hashtag
                hashtags.forEach(ht=>{
                    let contentht = `<span id="${ht.id}" class="linkHt">${ht.content}</span>`
                    htcontent.push(contentht)
                })
            }
            if (nbCom >= 1) {
                firstCom = `<div id="firstComm">`
                readCom = `<span id="comListe" data-idCom="${idCom}" data-id="${idMessage}">  liste des commentaires: <i class="fas fa-caret-down"></i></span>`
            }else{
                firstCom = `<div id="firstComm" class="hide">`
                readCom = ''
            }

            if (window.location.href.indexOf("perso") > -1) {
                post = `<div class="post" data-id="${author}" data-idmsg="${idMessage}">`
                view = `<span id="view" data-id="${author}" data-idmsg="${idMessage}">  Voir </span>`
            }else{
                post = `<div class="post${idMessage}" data-id="${author}" data-idmsg="${idMessage}">`
                view = `<a href="/perso/${idUser}"> Retour </a>`
            }



            $('#postSection').append(`
                    ${post}
                    <div class="twitTopBarPost">
                    <img id="author${author}"  data-id="${author}" class="circleIdPost" src="${image}" alt="${image}">
                    <div class="nomPost">${nom}</div>
                <div class="prenomPost">${prenom}</div>
                <div class="createdAt">${date.toLocaleString()}</div>
            </div>
           

<div class="contenu2">
${content}</br>
${htcontent}
</div>

            <div class="post-actions">
            <div class="nbComCount${idMessage}"><i data-id="${idMessage}" data-user="${author}" class="far rot25Deg transition fa-comment"></i>&nbsp ${nbCom}</div>
            <i data-id="${idMessage}" data-user="${author}" class="fas rot25Deg transition fa-retweet"></i>


             
            <div  id="nblike"><i id="message${idMessage}" class="rot25Deg transition far fa-heart ${correllation ? 'fas coeurAnim' : 'far'}" data-id="${idMessage}" data-user="${author}" data-like="${nbLike}"></i>&nbsp<span class="likesNb${idMessage}"> ${nbLike}</span><span style="font-size: 10px;"> &nbsp Likes</span> </div></div>


            <div id="inputCommentCache${idMessage}" class="inputCommentCache">

            <form id="sendComment" data-id="${idMessage}">
            <input id="inputComment${idMessage}" class="inputComment" type="text" placeholder="Votre commentaire">
            <button id="btnSendComment" type="submit"  data-idmsg="${idMessage}" class="fas  paperPlaneComment fa-paper-plane"></button>
            </form>
            </div>
            
            <div id="insertComForMess${idMessage}" class="insertCom">

            ${firstCom}
            <div id="firstUserCom">
            <img class="circleIdPostCom" src="${comPhoto}">
            <div id="profilRepCom">
            ${comNom}   <br/>
            ${comPrenom}

            </div>
            <div id="emplacementcom">
            ${commentaire}
            </div>
            </div>
            </div>
            </div>
            ${readCom}
            ${view}

            `);

            htcontent = [];

        })




    })
}
if (window.location.href.indexOf("perso") > -1) {
    refeshHome(forAll)
}else{
    var urlId = getUrlVars()["id"];
    refeshHome(forHim+urlId)
}



$(document).on('click','.fa-heart',  function(e){

    let idDivMessage = e.target.id
    let likes =  $(`#${idDivMessage}`).data('like')
    let idMessage =$(`#${idDivMessage}`).data('id')

    $.ajax({
        type: "POST",
        url: `/api/like`,
        data: {'message_id': idMessage, 'user_id': idUser},
        datatype: 'json',

        success: function (msg) {

            $(`#${idDivMessage}`).css(
                {'transform' : 'rotate(360deg)',})

            $(`#${idDivMessage}`).removeClass('far')
            $(`#${idDivMessage}`).addClass('fas')
            $(`#${idDivMessage}`).addClass('coeurAnim')
            $(`.likesNb${idMessage}`).text(likes + 1)



        },
        error: function(){
            console.log('erreur de data')
        }
    })

})

$(document).on('click', '.modif', function (e) {

    if (!profilmodif) {
        const divUsername = $('#usernameProfil');
        const value = divUsername.text()
        divUsername.html(`<input id="newUsername" type="text" placeholder="New Username..." value="${value}">`)
        $('.modif').removeClass('fa-user-edit').addClass('fa-save')
        profilmodif = true
    } else {
        const username = $('#newUsername').val()
        if (username) {
            $('#usernameProfil').html($('#newUsername').val())
            $('.modif').removeClass('fa-save').addClass('fa-user-edit')

            $.ajax({
                type: 'PUT',
                url: `/api/users/${idUser}`,
                data: { 'username': username },
                datatype: 'json',

                success: function (msg) {

                },
                error: function () {
                    console.log('erreur de data')
                }
            })
            profilmodif = !profilmodif
        } else {
            $('#usernameProfil > input').css('border', '1px solid red').attr('placeholder', 'Invalid').css('color', 'red');
        }
    }
})

setImageProfil()