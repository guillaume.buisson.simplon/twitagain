<?php

namespace App\Tests\Controller;

use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase
{
    public function testNotLoggedInAdminDashobard()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/admin');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testAdminDashobard()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UsersRepository::class);
        $testUser = $userRepository->findOneByEmail('admin@caramail.com');
        $client->loginUser($testUser);
        $crawler = $client->request('GET', '/admin');

        $this->assertResponseIsSuccessful();
    }
}
