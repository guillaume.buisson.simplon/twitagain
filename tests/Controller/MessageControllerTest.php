<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MessageControllerTest extends WebTestCase
{
    public function testGetAllMessages()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('messages', $responseData);
        $this->assertArrayHasKey('id', $responseData['messages'][0]);
        $this->assertArrayHasKey('content', $responseData['messages'][0]);
        $this->assertArrayHasKey('createdat', $responseData['messages'][0]);
        $this->assertArrayHasKey('image', $responseData['messages'][0]);
        $this->assertArrayHasKey('user', $responseData['messages'][0]);
        $this->assertArrayHasKey('likes', $responseData['messages'][0]);
    }

    public function testGetOneMessage()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/1');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('message', $responseData);
        $this->assertArrayHasKey('id', $responseData['message'][0]);
        $this->assertArrayHasKey('content', $responseData['message'][0]);
        $this->assertArrayHasKey('author_id', $responseData['message'][0]);
        $this->assertArrayHasKey('createdat', $responseData['message'][0]);
    }

    public function testGetOneMessageNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/99999999');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testUpdateMessage()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/messages/1', [
            'content' => 'test',
            'image' => 'image.png'
        ]);
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('status', $responseData);
    }

    public function testUpdateMessageNotFound()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/messages/9999999', [
            'content' => 'test',
            'image' => 'image.png'
        ]);
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testCreateAndDeleteMessage()
    {
        $client = static::createClient();
        $client->request('POST', '/api/messages', [
            'content' => 'test',
            'user' => 1
        ]);
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);
        $this->assertArrayHasKey('message', $responseData);
        $this->assertArrayHasKey('id', $responseData);

        $client->request('DELETE', '/api/messages/' . $responseData['id']);
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('mess', $responseData);
    }

    public function testAddMessageUserNotFound()
    {
        $client = static::createClient();
        $client->request('POST', '/api/messages', [
            'content' => 'test',
            'user' => 9999999
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testDeleteMessageNotFound()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/messages/999999');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);

    }

    public function testGetMessageLikes()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/1/likes');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('message', $responseData);
    }

    public function testGetMessageLikesNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/9999999/likes');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testGetMessageRT()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/1/retweets');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('message', $responseData);
    }

    public function testGetMessageRTNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/9999999/retweets');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testGetMessageComments()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/1/comments');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('message', $responseData);
    }

    public function testGetMessageCommentsNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/999999/comments');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testGetMessageHashtags()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/1/hashtags');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('message', $responseData);
    }

    public function testGetMessageHashtagsNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/messages/999999/hashtags');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }
}
