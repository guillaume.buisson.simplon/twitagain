<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CommentControllerTest extends WebTestCase
{
    public function testCompleteComment()
    {
        $client = static::createClient();
        // Add
        $client->request('POST', '/api/comments', [
            'content' => 'test',
            'user_id' => '2',
            'message_id' => '3',
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('comment', $responseData);
        $this->assertArrayHasKey('id', $responseData);

        $newId = $responseData['id'];

        // Edit
        $client->request('PUT', '/api/comments/' . $newId, [
            'content' => 'test',
        ]);
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('status', $responseData);

        // Delete
        $client->request('DELETE', '/api/comments/' . $newId);
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('comment', $responseData);
    }

    public function testAddCommentUserNotFound()
    {
        $client = static::createClient();
        $client->request('POST', '/api/comments', [
            'content' => 'test',
            'user_id' => '9999999',
            'message_id' => '3',
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testAddCommentMessageNotFound()
    {
        $client = static::createClient();
        $client->request('POST', '/api/comments', [
            'content' => 'test',
            'user_id' => '2',
            'message_id' => '999999999',
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testUpdateCommentNotFound()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/comments/99999999', [
            'content' => 'test',
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testDeleteCommentNotFound()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/comments/99999999');

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }
}
