<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LikeControllerTest extends WebTestCase
{
    public function testAddLike()
    {
        $client = static::createClient();
        $client->request('POST', '/api/like', [
            'user_id' => 3,
            'message_id' => 2
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('like_success', $responseData);
    }

    public function testAddLikeMessageNotFound()
    {
        $client = static::createClient();
        $client->request('POST', '/api/like', [
            'user_id' => 3,
            'message_id' => 999999999
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testAddLikeUserNotFound()
    {
        $client = static::createClient();
        $client->request('POST', '/api/like', [
            'user_id' => 9999999,
            'message_id' => 2
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testDeleteLike()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/likes/2', [
            'user_id' => 3,
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('comment', $responseData);
    }

    public function testDeleteLikeMessageNotFound()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/likes/999999', [
            'user_id' => 3,
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }
}
