<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testGetUsers()
    {
        $client = static::createClient();
        $client->request('GET', '/api/users');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('users', $responseData);
        $this->assertCount(31, $responseData['users']); // 31 because 30 basic user + 1 admin in fixtures
    }

    public function testGetOneUser()
    {
        $client = static::createClient();
        $client->request('GET', '/api/users/1');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('users', $responseData);
        $this->assertArrayHasKey('id', $responseData['users']);
        $this->assertArrayHasKey('email', $responseData['users']);
        $this->assertArrayHasKey('nom', $responseData['users']);
        $this->assertArrayHasKey('prenom', $responseData['users']);
        $this->assertArrayHasKey('username', $responseData['users']);
        $this->assertArrayHasKey('date', $responseData['users']);
    }

    public function testPostUsers()
    {
        $client = static::createClient();
        $client->request('POST', '/api/users', [
            "email" => "test@test.fr",
            "nom" => "Doe",
            "prenom" => "John",
            "pwd" => "password",
            "username" => "jdoe",
        ]);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(201);
        $this->assertArrayHasKey('users', $responseData);

        $client->request('DELETE', '/api/users/' . $responseData['id']);
        $this->assertResponseIsSuccessful();

    }

    public function testRemoveUserNotFound()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/users/999999');
        $responseData = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testGetUsersMessages()
    {
        $client = static::createClient();
        $client->request('GET', '/api/users/2/messages');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('messages', $responseData);
        $this->assertArrayHasKey('id', $responseData['messages'][0]);
        $this->assertArrayHasKey('content', $responseData['messages'][0]);
        $this->assertArrayHasKey('Date', $responseData['messages'][0]);
        $this->assertArrayHasKey('auteur', $responseData['messages'][0]);
    }

    public function testGetUsersFollowers()
    {
        $client = static::createClient();
        $client->request('GET', '/api/users/2/followers');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('user', $responseData);
        $this->assertArrayHasKey('id', $responseData['user']);
        $this->assertArrayHasKey('followers', $responseData['user']);
    }

    public function testGetUsersFollowersNotFound()
    {
        $client = static::createClient();
        $client->request('GET', '/api/users/999999/followers');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }

    public function testUpdateUser()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/users/2', [
            'username' => 'test',
            'image' => 'test.png'
        ]);
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('user', $responseData);
    }

    public function testUpdateUserNotFound()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/users/99999999', [
            'username' => 'test',
            'image' => 'test.png'
        ]);
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('error', $responseData);
    }
}
