<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InscriptionControllerTest extends WebTestCase
{
    public function testInscription()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/inscription');

        $this->assertResponseIsSuccessful();
        $this->assertPageTitleSame("Bienvenue sur TweetAgain !");
    }

    public function testConnexion()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/connexion');

        $this->assertResponseIsSuccessful();
        $this->assertPageTitleSame("Bienvenue sur TweetAgain !");
    }

    public function testConnexionAjax()
    {
        $client = static::createClient();
        $client->request('POST', '/connexionAjax', [], [], ['CONTENT_TYPE' => 'application/json'],'{"email":"admin@caramail.com", "password":"admin"}');
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('result', $responseData);
        $this->assertArrayHasKey('id', $responseData);
        $this->assertEquals(1, $responseData['id']);
    }

    public function testConnexionAjaxFailed()
    {
        $client = static::createClient();
        $client->request('POST', '/connexionAjax', []);
        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertResponseStatusCodeSame(404);
        $this->assertArrayHasKey('result', $responseData);
    }

    public function testConnexionAjaxPasswordFailed()
    {
        $client = static::createClient();
        $client->request('POST', '/connexionAjax', [], [], ['CONTENT_TYPE' => 'application/json'],'{"email":"admin@caramail.com", "password":"wrong"}');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testLogout()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/logout');
        $this->assertResponseStatusCodeSame(302);
    }
}
