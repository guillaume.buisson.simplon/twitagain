<?php

namespace App\Tests\Entity;

use App\Entity\Comments;
use App\Entity\Messages;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class UsersTest extends TestCase
{
    public function testCompleteEntityCreation()
    {
        $date = new \DateTime('2020-01-01 00:00:00');
        $message = new Messages();
        $comment = new Comments();
        $userFollow = new Users();

        $user = new Users();
        $user->setEmail('test@test.fr');
        $user->setNom('Doe');
        $user->setPrenom('John');
        $user->setPassword('password');
        $user->setUsername('jdoe');
        $user->setImage('image.jpg');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setCreatedat($date);

        $user->addComments($comment);
        $user->addRetweet($message);
        $user->addMention($message);
        $user->addLike($message);
        $user->addFollower($userFollow);
        $user->addMessage($message);

        $this->assertEquals('test@test.fr', (string) $user);

        $this->assertEquals('test@test.fr', $user->getEmail());
        $this->assertEquals('Doe', $user->getNom());
        $this->assertEquals('John', $user->getPrenom());
        $this->assertEquals('password', $user->getPassword());
        $this->assertEquals('jdoe', $user->getUsername());
        $this->assertEquals('image.jpg', $user->getImage());
        $this->assertEquals($date, $user->getCreatedat());
        $this->assertEquals(['ROLE_ADMIN', 'ROLE_USER'], $user->getRoles());


        $this->assertContains($comment, $user->getComments());
        $this->assertContains($message, $user->getRetweets());
        $this->assertContains($message, $user->getMentions());
        $this->assertContains($message, $user->getLikes());
        $this->assertContains($userFollow, $user->getFollowers());
        $this->assertContains($message, $user->getMessages());

        $user->removeComments($comment);
        $user->removeRetweet($message);
        $user->removeMention($message);
        $user->removeLike($message);
        $user->removeFollower($userFollow);
        $user->removeMessage($message);

        $this->assertNotContains($comment, $user->getComments());
        $this->assertNotContains($message, $user->getRetweets());
        $this->assertNotContains($message, $user->getMentions());
        $this->assertNotContains($message, $user->getLikes());
        $this->assertNotContains($userFollow, $user->getFollowers());
        $this->assertNotContains($message, $user->getMessages());

    }
}
