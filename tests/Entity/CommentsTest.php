<?php

namespace App\Tests\Entity;

use App\Entity\Comments;
use App\Entity\Messages;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class CommentsTest extends TestCase
{
    public function testCompleteEntityCreation()
    {
        $date = new \DateTime('2020-01-01 00:00:00');
        $user = new Users();
        $message = new Messages();

        $comment = new Comments();
        $comment->setTextcom('test');
        $comment->setCreatedat($date);
        $comment->setMessage($message);
        $comment->setUser($user);

        $this->assertEquals('test', $comment->getTextcom());
        $this->assertEquals($date, $comment->getCreatedat());
        $this->assertEquals($user, $comment->getUser());
        $this->assertEquals($message, $comment->getMessage());
        $this->assertEquals($comment->getId(), (string) $comment);

    }
}
