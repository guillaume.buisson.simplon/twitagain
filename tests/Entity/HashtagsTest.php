<?php

namespace App\Tests\Entity;

use App\Entity\Hashtags;
use App\Entity\Messages;
use PHPUnit\Framework\TestCase;

class HashtagsTest extends TestCase
{
    public function testCompleteEntityCreation()
    {
        $message = new Messages();

        $hashtag = new Hashtags();
        $hashtag->setTexthashtag('test');
        $hashtag->addMsgid($message);

        $this->assertEquals('test', $hashtag->getTexthashtag());
        $this->assertTrue($hashtag->getMsgid()->contains($message));
        $this->assertEquals('test', (string) $hashtag);

        $hashtag->removeMsgid($message);
        $this->assertFalse($hashtag->getMsgid()->contains($message));
    }
}
