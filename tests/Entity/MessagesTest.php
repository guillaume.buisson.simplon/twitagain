<?php

namespace App\Tests\Entity;

use App\Entity\Comments;
use App\Entity\Hashtags;
use App\Entity\Messages;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class MessagesTest extends TestCase
{
    public function testCompleteEntityCreation()
    {
        $date = new \DateTime('2020-01-01 00:00:00');

        $user = new Users();
        $comment = new Comments();
        $hastag = new Hashtags();

        $message = new Messages();
        $message->setContent('Test');
        $message->setImage('test.png');
        $message->setCreatedat($date);
        $message->setAuthor($user);
        $message->addLike($user);
        $message->addMention($user);
        $message->addRetweet($user);
        $message->addComments($comment);
        $message->addHashtag($hastag);


        $this->assertEquals('Test', $message->getContent());
        $this->assertEquals('test.png', $message->getImage());
        $this->assertEquals($date->getTimestamp(), $message->getCreatedat()->getTimestamp());
        $this->assertEquals($user, $message->getAuthor());
        $this->assertTrue($message->getLikes()->contains($user));
        $this->assertTrue($message->getMentions()->contains($user));
        $this->assertTrue($message->getRetweets()->contains($user));
        $this->assertTrue($message->getHashtags()->contains($hastag));
        $this->assertTrue($message->getComments()->contains($comment));

        $this->assertEquals($message->getId(), (string) $message);

        $message->removeHashtag($hastag);
        $message->removeLike($user);
        $message->removeMention($user);
        $message->removeRetweet($user);
        $message->removeComments($comment);

        $this->assertFalse($message->getLikes()->contains($user));
        $this->assertFalse($message->getMentions()->contains($user));
        $this->assertFalse($message->getRetweets()->contains($user));
        $this->assertFalse($message->getHashtags()->contains($hastag));
        $this->assertFalse($message->getComments()->contains($comment));
    }
}
