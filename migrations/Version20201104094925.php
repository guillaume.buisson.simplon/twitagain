<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201104094925 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comments (id INT AUTO_INCREMENT NOT NULL, message_id INT NOT NULL, user_id INT NOT NULL, textcom VARCHAR(255) NOT NULL, createdat DATETIME NOT NULL, INDEX IDX_5F9E962A537A1329 (message_id), INDEX IDX_5F9E962AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hashtags (id INT AUTO_INCREMENT NOT NULL, texthashtag VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hashtags_messages (hashtags_id INT NOT NULL, messages_id INT NOT NULL, INDEX IDX_DD7610FC65827D0B (hashtags_id), INDEX IDX_DD7610FCA5905F5A (messages_id), PRIMARY KEY(hashtags_id, messages_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messages (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, content LONGTEXT NOT NULL, createdat DATETIME NOT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_DB021E96F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE likes (messages_id INT NOT NULL, users_id INT NOT NULL, INDEX IDX_49CA4E7DA5905F5A (messages_id), INDEX IDX_49CA4E7D67B3B43D (users_id), PRIMARY KEY(messages_id, users_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mentions (messages_id INT NOT NULL, users_id INT NOT NULL, INDEX IDX_FE39735FA5905F5A (messages_id), INDEX IDX_FE39735F67B3B43D (users_id), PRIMARY KEY(messages_id, users_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE retweets (messages_id INT NOT NULL, users_id INT NOT NULL, INDEX IDX_4923CB40A5905F5A (messages_id), INDEX IDX_4923CB4067B3B43D (users_id), PRIMARY KEY(messages_id, users_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, createdat DATE NOT NULL, username VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE followers (users_source INT NOT NULL, users_target INT NOT NULL, INDEX IDX_8408FDA7506DF1E3 (users_source), INDEX IDX_8408FDA74988A16C (users_target), PRIMARY KEY(users_source, users_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A537A1329 FOREIGN KEY (message_id) REFERENCES messages (id)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE hashtags_messages ADD CONSTRAINT FK_DD7610FC65827D0B FOREIGN KEY (hashtags_id) REFERENCES hashtags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hashtags_messages ADD CONSTRAINT FK_DD7610FCA5905F5A FOREIGN KEY (messages_id) REFERENCES messages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E96F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE likes ADD CONSTRAINT FK_49CA4E7DA5905F5A FOREIGN KEY (messages_id) REFERENCES messages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE likes ADD CONSTRAINT FK_49CA4E7D67B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mentions ADD CONSTRAINT FK_FE39735FA5905F5A FOREIGN KEY (messages_id) REFERENCES messages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mentions ADD CONSTRAINT FK_FE39735F67B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE retweets ADD CONSTRAINT FK_4923CB40A5905F5A FOREIGN KEY (messages_id) REFERENCES messages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE retweets ADD CONSTRAINT FK_4923CB4067B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE followers ADD CONSTRAINT FK_8408FDA7506DF1E3 FOREIGN KEY (users_source) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE followers ADD CONSTRAINT FK_8408FDA74988A16C FOREIGN KEY (users_target) REFERENCES users (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE hashtags_messages DROP FOREIGN KEY FK_DD7610FC65827D0B');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962A537A1329');
        $this->addSql('ALTER TABLE hashtags_messages DROP FOREIGN KEY FK_DD7610FCA5905F5A');
        $this->addSql('ALTER TABLE likes DROP FOREIGN KEY FK_49CA4E7DA5905F5A');
        $this->addSql('ALTER TABLE mentions DROP FOREIGN KEY FK_FE39735FA5905F5A');
        $this->addSql('ALTER TABLE retweets DROP FOREIGN KEY FK_4923CB40A5905F5A');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962AA76ED395');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E96F675F31B');
        $this->addSql('ALTER TABLE likes DROP FOREIGN KEY FK_49CA4E7D67B3B43D');
        $this->addSql('ALTER TABLE mentions DROP FOREIGN KEY FK_FE39735F67B3B43D');
        $this->addSql('ALTER TABLE retweets DROP FOREIGN KEY FK_4923CB4067B3B43D');
        $this->addSql('ALTER TABLE followers DROP FOREIGN KEY FK_8408FDA7506DF1E3');
        $this->addSql('ALTER TABLE followers DROP FOREIGN KEY FK_8408FDA74988A16C');
        $this->addSql('DROP TABLE comments');
        $this->addSql('DROP TABLE hashtags');
        $this->addSql('DROP TABLE hashtags_messages');
        $this->addSql('DROP TABLE messages');
        $this->addSql('DROP TABLE likes');
        $this->addSql('DROP TABLE mentions');
        $this->addSql('DROP TABLE retweets');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE followers');
    }
}
